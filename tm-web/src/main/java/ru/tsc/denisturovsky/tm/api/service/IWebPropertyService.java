package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IWebPropertyService {

    @NotNull
    String getDBConfigFilePath();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBFactoryClass();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBRegionPrefix();

    @NotNull
    String getDBSecondLvlCache();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBUseQueryCache();

    @NotNull
    String getDBUser();

}
