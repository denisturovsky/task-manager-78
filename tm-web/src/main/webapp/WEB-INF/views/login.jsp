<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>

<form name="f" action="/auth" method="POST">
    <table style="width: 50%;" align="center">
        <tr>
            <td colspan="2">
                <h3>Login with username and password</h3>
            </td>
        </tr>
        <tr>
            <td>Username:</td>
            <td><input type="text" name="username" value=""></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password" value=""></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" type="submit" value="login">
            </td>
        </tr>
    </table>
</form>

<jsp:include page="../include/_footer.jsp"/>