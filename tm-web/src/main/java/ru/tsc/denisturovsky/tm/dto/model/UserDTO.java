package ru.tsc.denisturovsky.tm.dto.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_user")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userDTO", propOrder = {
        "id",
        "login",
        "passwordHash"
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    public UserDTO(
            @NotNull String login,
            @NotNull String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}