package ru.tsc.denisturovsky.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.marker.WebUnitCategory;
import ru.tsc.denisturovsky.tm.repository.ProjectDTORepository;
import ru.tsc.denisturovsky.tm.repository.TaskDTORepository;
import ru.tsc.denisturovsky.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final TaskDTO task1 = new TaskDTO(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);

    @NotNull
    private final TaskDTO task2 = new TaskDTO(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @After
    @SneakyThrows
    public void after() {
        taskRepository.delete(task1);
        taskRepository.delete(task2);
        projectRepository.delete(project1);
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken
                token = new UsernamePasswordAuthenticationToken(USER_TEST_NAME, USER_TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        projectRepository.save(project1);
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByUserIdAndId(userId, task1.getId());
        Assert.assertEquals(1, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdTest() {
        taskRepository.deleteByUserId(userId);
        Assert.assertEquals(0, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void existByUserIdAndIdTest() {
        Assert.assertFalse(taskRepository.existByUserIdAndId("", task1.getId()));
        Assert.assertFalse(taskRepository.existByUserIdAndId(userId, ""));
        Assert.assertTrue(taskRepository.existByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void findByProjectIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByProjectId(project1.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        @NotNull final Optional<TaskDTO> task = taskRepository.findByUserIdAndId(userId, task1.getId());
        Assert.assertEquals(task1.getId(), task.orElse(null).getId());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndProjectIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByUserIdAndProjectId(userId, project1.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByUserId(userId);
        Assert.assertEquals(2, tasks.size());
    }

}